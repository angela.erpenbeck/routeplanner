import sys

journey = {}
def process():
    print("This is a routemap planner for the germany long-distance railway trains. Please anwser following questions for your request. Write the City with the correct writing." )
    
    print('-> Please enter your starting point. Confirm with <Enter>')
    start_var = input(">> Start(Default Osnabrück Hbf): ")
    if start_var == "":
        start_var = "Osnabrück Hbf"
    
    print('-> Please enter your destination. Confirm with <Enter>')
    end_var = input(">> End (Default Berlin Hbf): ")
    if end_var == "":
        end_var = "Berlin Hbf"
        
    print('-> What weekday do you want to travel. Write one day from the following short definitions [mo,di,mi,do,fr,sa,so]. Confirm with <Enter>')
    weekday_var = input(">> Weekday(Default mo): ")
    if weekday_var == "":
        weekday_var = "mo"
        
    print('-> At what time do you want to travel. Write 8:00 for 8 am and 20:00 for 8 pm. Confirm with <Enter>')
    time_var = input(">> Time (Default 8:00): ")
    if time_var == "":
        time_var = "8:00"
        
    journey= {"start": start_var, 
              "end": end_var, 
              "weekday": weekday_var,
              "time": time_var
              }

    print("--> You entered as Start: " + start_var +" and Destination: " + end_var + " for " + weekday_var + " at " + time_var)
    

    
    return journey
    
    
if __name__ == "__main__":
    """  
    """
    p =  process()

    
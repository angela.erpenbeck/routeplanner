# Copyright <2022> <Angela Erpenbeck, Ella Fink, Wiebke Janßen >
import pandas as pd
from pathlib import Path

#Defining path, where all needed files are
data_path = Path('Schienenfernverkehr_ICE_IC')

#opening first files to merge
file1 = data_path/'calendar_dates.txt'
file2 = data_path/'calendar.txt'

#reading the files with pandas
data1 = pd.read_csv(file1)
data2 = pd.read_csv(file2)

#merging the two first files into one dataframe
route_information = pd.merge(pd.DataFrame(data1), 
                             pd.DataFrame(data2), 
                             left_on = ['service_id'], 
                             right_on = ['service_id'], 
                             how = 'left')

#continuing to open and read files
file3 = data_path/'trips.txt'
data3 = pd.read_csv(file3)

#continuing to merge files into one
route_information = pd.merge(pd.DataFrame(data3),
                                pd.DataFrame(route_information),
                                left_on = ['service_id'], 
                                right_on = ['service_id'], 
                                how = 'left')
                                


file4 = data_path/'stop_times.txt'
data4 = pd.read_csv(file4)

route_information = pd.merge(pd.DataFrame(data4),
                                pd.DataFrame(route_information),
                                left_on = ['trip_id'], 
                                right_on = ['trip_id'], 
                                how = 'left')

file5 = data_path/'stops.txt'
data5 = pd.read_csv(file5)

route_information = pd.merge(pd.DataFrame(data5),
                                pd.DataFrame(route_information),
                                left_on = ['stop_id'], 
                                right_on = ['stop_id'], 
                                how = 'left')

#droping unneeded information from file and writing new file containing all route information
route_information.drop(['stop_lat', 'stop_lon'],inplace=True,axis=1)
route_information.to_csv('route_information.txt', index=False)
 
#source:https://www.analytics-link.com/post/2018/09/14/applying-the-a-path-finding-algorithm-in-python-part-2-2d-coordinate-pairs
 
##############################################################################

# import packages

##############################################################################


import numpy as np

import heapq

import pandas as pd

from collections import OrderedDict

 

##############################################################################

# coordinate pairs

##############################################################################

 

x1 = [3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,3,2,4,4,4,3]

y1 = [3,3,2,2,2,2,1,1,3,3,3,3,1,1,1,1,3,3,2,2,2,2,1,1,2,2,2,2,4,2]

x2 = [3,2,3,3,2,2,3,2,3,3,1,1,3,3,1,1,2,1,3,2,2,1,2,1,2,1,3,3,3,4]

y2 = [2,3,3,1,3,1,1,1,3,2,3,2,2,1,2,1,3,2,1,3,1,1,1,2,2,2,2,3,1,2]

 

coord_pairs = pd.DataFrame( OrderedDict((('x1', pd.Series(x1)), ('y1', pd.Series(y1)), ('x2', pd.Series(x2)), ('y2', pd.Series(y2)))))

coord_pairs = coord_pairs.sort_values(['x1', 'y1'], ascending=[True,True])

print(coord_pairs)

 

##############################################################################

# specify start and goal positions

##############################################################################

 

# start = (4,2)

# goal = (1,2)

 
start = (1,2)

goal = (4,2)

 

##############################################################################

# a* path finding functions

##############################################################################

 

def available_neighbours(current_x,current_y):

    return list(zip(coord_pairs.loc[(coord_pairs.x1 == current_x) & (coord_pairs.y1 == current_y)][["x2"]].x2, coord_pairs.loc[(coord_pairs.x1 == current_x) & (coord_pairs.y1 == current_y)][["y2"]].y2))

 

def heuristic(a, b):

    return np.sqrt((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2)

 

def astar(start, goal):

 

    close_set = set()

    came_from = {}

    gscore = {start:0}

    fscore = {start:heuristic(start, goal)}

    oheap = []

     

    heapq.heappush(oheap, (fscore[start], start))

    while oheap:

     

        current = heapq.heappop(oheap)[1]

        neighbours = available_neighbours(current[0],current[1])

         

    if current == goal:

        data = []

    while current in came_from:

        data.append(current)

        current = came_from[current]

        return data

     

    close_set.add(current)

    for i, j in neighbours:

        neighbour = i, j

        tentative_g_score = gscore[current] + heuristic(current, neighbour)

        if neighbour in close_set and tentative_g_score >= gscore.get(neighbour, 0):

            continue

    if tentative_g_score < gscore.get(neighbour, 0) or neighbour not in [i[1]for i in oheap]:

        came_from[neighbour] = current

        gscore[neighbour] = tentative_g_score

        fscore[neighbour] = tentative_g_score + heuristic(neighbour, goal)

        heapq.heappush(oheap, (fscore[neighbour], neighbour))

    return False

 

##############################################################################

# calculate route

##############################################################################

     

    route = astar(start, goal)

    route = route + [start]

    route = route[::-1]

    print(route)

 